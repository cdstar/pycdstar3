DOCS_ALL = $(shell find docs/ -type f)
SRC_ALL = $(shell find src/ -type f -name '*.py')

venv: venv/touch-me
venv/touch-me: pyproject.toml
	test -d venv || python3 -mvenv venv
	venv/bin/pip install -U pip -e .[dev,test,docs]
	touch venv/touch-me

docs: build/docs/index.html
build/docs/index.html: venv $(DOCS_ALL) $(SRC_ALL)
	venv/bin/sphinx-build -a docs/ build/docs/

dist: venv $(SRC_ALL)
	venv/bin/hatch build

lint: venv
	venv/bin/flake8 src/
	venv/bin/black --check src/ tests/

format: venv
	venv/bin/black src/ tests/

test: venv
	venv/bin/pytest

coverage: venv
	venv/bin/pytest --cov=pycdstar3 --cov-branch  --cov-report=term --cov-report=html:build/htmlcov

push: test lint
	git diff-index --exit-code HEAD -- # Ensure workdir and index are both clean
	git push

_PHONY: venv docs dist lint test coverage push clean format

clean:
	rm -rf build dist venv .pytest_cache .coverage MANIFEST
	find src -name '*.pyc' -or -name '__pycache__' -delete
