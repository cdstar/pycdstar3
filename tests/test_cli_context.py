import os
from argparse import Namespace

import pytest

from pycdstar3.cli.context import CliContext, Workspace
from pycdstar3.model import JsonObject


@pytest.fixture
def workspace(tmpdir):
    work = Workspace(tmpdir)
    work.store_config(server="http://example.org/v3/", vault="test")
    return Workspace(tmpdir)


@pytest.fixture
def context(workspace):
    args = Namespace(quiet=False, verbose=0)
    return CliContext(args, workspace.root)


def test_CliContext_override_settings(tmpdir):
    """Environment variables override workspace settings and command-line arguments override both."""
    args = Namespace(quiet=False, verbose=0)
    Workspace(tmpdir).store_config(server="http://work/v3/", vault="work")

    ctx = CliContext(args, tmpdir)
    assert "work" in ctx.client.url
    assert ctx.vault == "work"

    os.environ["CDSTAR_SERVER"] = "https://env/v3/"
    os.environ["CDSTAR_VAULT"] = "env"

    ctx = CliContext(args, tmpdir)
    assert "env" in ctx.client.url
    assert ctx.vault == "env"

    args.server = "https://args/v3/"
    args.vault = "args"

    ctx = CliContext(args, tmpdir)
    assert "args" in ctx.client.url
    assert ctx.vault == "args"


def test_CliContext_client(context):
    assert context.client
