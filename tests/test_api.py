import pytest
import responses

from pycdstar3 import CDStar, ApiError

SERVER = "https://example.com/test/v3/"


@pytest.fixture
def restmock():
    with responses.RequestsMock() as rsps:
        yield rsps


@pytest.fixture
def client():
    return CDStar(SERVER)


def test_service_info(client, restmock):
    restmock.add("GET", SERVER, json={"version": {"api": "3.1"}})
    assert client.service_info().version.api == "3.1"


def test_vault_info(client, restmock):
    restmock.add("GET", SERVER + "test", json={"public": True})
    assert client.vault_info("test").public is True


def test_tx(client, restmock):
    with pytest.raises(RuntimeError):
        # Can't commit without transaction!
        client.commit()

    with pytest.raises(RuntimeError):
        # Cant't call keepalive without transaction!
        client.keepalive()

    txjson = {
        "id": "123",
        "ttl": 60,
        "timeout": 60,
        "isolation": "snapshot",
        "readonly": False,
    }

    restmock.add("POST", SERVER + "_tx", json=txjson)
    with client.begin():
        assert client.tx.id == "123"

        restmock.add("POST", SERVER + "_tx/123?renew=True", json=txjson)
        client.keepalive()

        restmock.add("POST", SERVER + "_tx/123")
        client.commit()
        assert client.tx is None

    with client.begin():
        restmock.add("DELETE", SERVER + "_tx/123")
        client.rollback()
        assert client.tx is None

    with client.begin():
        pass
    assert restmock.calls[-1].request.method == "DELETE"
    assert restmock.calls[-1].request.url == SERVER + "_tx/123"

    with client.begin(autocommit=True):
        pass
    assert restmock.calls[-1].request.method == "POST"
    assert restmock.calls[-1].request.url == SERVER + "_tx/123"

    with pytest.raises(RuntimeError):
        with client:
            pass  # pragma: no cover


def test_error(client, restmock):
    with pytest.raises(ApiError) as e:
        restmock.add(
            "GET",
            SERVER + "missing",
            status=404,
            json={
                "status": 404,
                "error": "VaultNotFound",
                "message": "Vault not found",
                "detail": {"vault": "bad"},
            },
        )
        client.rest("GET", "missing")

    assert e.value.status == 404
    assert e.value.error == "VaultNotFound"
