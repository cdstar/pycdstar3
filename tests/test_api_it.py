"""

Integration tests of the client library against a live CDSTAR server.

- Most tests use a transaction which is never committed, so they do not actually create any persistent data on the server.

"""

import hashlib
import os

import pytest

from pycdstar3 import CDStar

SERVER = os.environ.get("TEST_CDSTAR")
VAULT = os.environ.get("TEST_VAULT", "test")

pytestmark = pytest.mark.skipif(not SERVER, reason="No TEST_SERVER configured")


def test_service_info():  # pragma: no cover
    c = CDStar(SERVER)
    si = c.service_info()
    assert si["version"]["api"] in ("3.0", "3.1")


def test_tx():  # pragma: no cover
    c = CDStar(SERVER)
    with c.begin():
        assert "id" in c.tx
        assert c.tx["isolation"] == "snapshot"
        assert c.tx["readonly"] is False
        assert c.tx["timeout"] > 0
    assert c.tx is None


def test_tx_commit():  # pragma: no cover
    c = CDStar(SERVER)
    with c.begin():
        c.rollback()
        assert c.tx is None
    assert c.tx is None


def test_vault_info():  # pragma: no cover
    c = CDStar(SERVER)
    vi = c.vault_info(VAULT)
    assert "public" in vi


def test_crud_file():  # pragma: no cover
    c = CDStar(SERVER)

    with c.begin():
        a = c.create_archive(VAULT)
        with open(__file__, "rb") as fp:
            hash = hashlib.md5(fp.read()).hexdigest()
            fp.seek(0)
            r = c.put_file(VAULT, a["id"], "test.py", fp, type="text/x-python")
            assert r["digests"]["md5"] == hash

            assert r == c.file_info(VAULT, a["id"], "/test.py", meta=False)
            assert "test.py" == next(c.iter_files(VAULT, a["id"]))["name"]

            fget = c.get_file(VAULT, a["id"], "test.py")
            fp.seek(0)
            assert fp.read() == fget.readall()
            assert "text/x-python" == fget.type
            # no commit
