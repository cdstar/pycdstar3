API reference
=====================================

.. automodule:: pycdstar3


API Client (low-level)
----------------------
.. autoclass:: pycdstar3.api.CDStar
   :members:

Resource Handles (fluent API)
-----------------------------

.. autoclass:: pycdstar3.api.CDStarVault
   :members:

.. autoclass:: pycdstar3.api.CDStarArchive
   :members:

.. autoclass:: pycdstar3.api.CDStarFile
   :members:

Data Classes and Wrappers
-------------------------

.. automodule:: pycdstar3.model
   :members:
